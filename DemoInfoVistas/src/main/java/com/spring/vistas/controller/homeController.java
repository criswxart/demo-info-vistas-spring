package com.spring.vistas.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class homeController {

	ArrayList lista;
	@RequestMapping(value = {"/home","/"}, method = RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	public homeController() {
	  lista = new ArrayList<>();
	}
	@RequestMapping(value = {"/procesar","/"}, method = RequestMethod.POST)
	public String enviarDatos(ModelMap model,
			@RequestParam(value = "txtRut", required = false) String rut,
			@RequestParam(value = "txtNombre", required = false) String nombre,
			@RequestParam(value = "txtEdad", required = false) int edad
			) {
		//lógica de programación
		lista.add(rut);
		lista.add(nombre);
		lista.add(edad);
		System.out.println("Datos recibidos desde la vista: ");
		System.out.println("Rut: " + rut);
		System.out.println("Nombre: " + nombre);
		System.out.println("Edad: " + edad);
		
		model.put("listado", lista);
		
		return "procesar";
	}
}
