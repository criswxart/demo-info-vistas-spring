package com.spring.vistas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoInfoVistasApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoInfoVistasApplication.class, args);
	}

}
